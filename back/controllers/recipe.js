const recipeModel = require('../models/recipe')
const userModel = require('../models/user')
const mongoose = require('mongoose')


module.exports.createRecipe = async (req,res) => {

    
    let user = await (await userModel.findOne({ _id: req.params.id })).populate('recipes')

    let newRecipe = new recipeModel()

    newRecipe.name = req.body.name
    newRecipe.ingredients = req.body.ingredients
    newRecipe.method = req.body.method
    newRecipe.user = req.params.id

    const recipeCreated = await newRecipe.save()

    if(!recipeCreated){
        res.send('A receita não pode ser criada')
        return
    }


    user.recipes.push(recipeCreated)
    user.save((error, recipeSaved) => {
        if(error){
            console.log('Receita não pode ser salva.')
        }
        console.log('Receita foi salva! ' + recipeSaved)
    })

    res.json(recipeCreated)
    
}

module.exports.getRecipes = async (req,res) => {

    const recipes = await recipeModel.find()
    
    if(!recipes){
        res.send('Não há receitas')
    }

    res.json(recipes)
}


module.exports.getOneRecipe = async (req,res) => {

    const recipe = await recipeModel.findById(req.params.id)

    res.json(recipe)

}


module.exports.deleteRecipe = async (req,res) => {



    let recipe = await recipeModel.findOne({_id: req.params.id})

    const userId = recipe.user._id

    userModel.findOne({ _id: userId }).then( async (user) => {

        await user.recipes.pull({ _id: req.params.id })
        console.log(user.recipes)
        await user.save()
    })

    recipe = await recipeModel.findOneAndDelete({ _id: req.params.id })

    res.json(recipe)

}