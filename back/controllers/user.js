const userModel = require('../models/user')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')

const secret = "ohwellrecipes"

module.exports.createUser = async (req,res) => {


    if(!req.body.name){
        res.send("Campo obrigatório em branco!")
    }

    if(!req.body.email){
        res.send("Campo obrigatório em branco!")
    }

    if(!req.body.password){
        res.send("Campo obrigatório em branco!")
    }

    const salt = bcrypt.genSaltSync(10)
    const hashedPass = bcrypt.hashSync(req.body.password,salt)
    
    let newUser = new userModel()
    
    newUser.name = req.body.name
    newUser.email = req.body.email
    newUser.password = hashedPass

    const userCreated = await newUser.save()

    if(!userCreated){
        res.send("O usuário não pode ser criado")
    }

    res.json(userCreated)
}

module.exports.getUsers = async (req,res) => {


    const users = await userModel.find()

    res.json(users)

}

module.exports.getUser = async (req,res) => {

    const userFound = await userModel.findOne({_id: req.params.id})
    if(!userFound){
        res.send("Não foi possível encontrar")
    }

    res.json(userFound)
}


module.exports.login = async (req,res) => {

    if(!req.body.email){
        res.send("Campo obrigatório em branco")
    }


    if(!req.body.password){
        res.send("Campo obrigatório em branco")
    }

    userModel.findOne({email: req.body.email}, (error,userFound) => {

        if(!userFound){
            res.send("Usuário não encontrado. " + error)
        }
        
        if(bcrypt.compareSync(req.body.password,userFound.password)){
            res.json({
                token: jwt.sign({user: userFound},secret),
                email: req.body.email
            })
        }
        else{
            res.send("Senha incorreta!")
        }
    })
    
}