const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')

mongoose.connect('mongodb+srv://recipes:recipeme@recipes-cluster-bjyap.mongodb.net/test?retryWrites=true&w=majority',  { useNewUrlParser: true, useCreateIndex: true, useFindAndModify: false, useUnifiedTopology:true })


const userRoute = require('./routes/user')
const recipeRoute = require('./routes/recipe')

const PORT = process.env.PORT || 5000


const app = express()

app.use(bodyParser.json())

app.use(userRoute)
app.use(recipeRoute)

app.listen(PORT, () => {
    console.log(`Ouvindo na porta http://localhost:${PORT}.`)
})