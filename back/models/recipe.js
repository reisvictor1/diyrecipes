const mongoose = require('mongoose')


const recipeSchema = new mongoose.Schema({


    name: {
        type: String,
        required: true
    },

    ingredients: [{
        type: String,
        required: true
    }],

    method: {
        type: String,
        required: String
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    }

})


module.exports = mongoose.model('Recipe', recipeSchema)