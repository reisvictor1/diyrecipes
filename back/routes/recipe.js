const express = require('express')
const router = express.Router()
const recipeController = require('../controllers/recipe')

router.get('/recipe', recipeController.getRecipes)
router.post('/recipe/:id', recipeController.createRecipe)
router.get('/recipe/:id', recipeController.getOneRecipe)
router.delete('/recipe/:id', recipeController.deleteRecipe)

//router.get('/:id/recipes', recipeController.getUserRecipes)

module.exports = router