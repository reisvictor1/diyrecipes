import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles(theme => ({
  root: {
  },
  nav: {
      backgroundColor: "#00ffff"
  },
  menuButton: {
    color: 'black',
    fontSize: 10
  },
  title: {
    color: 'black',
    fontSize: 12,
  },

}));

export default function NavBar() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar  className={classes.nav} position="static">
        <Toolbar >
          <Typography className={classes.title}>
            News
          </Typography>
          <Button className={classes.menuButton} color="inherit">Login</Button>
        </Toolbar>
      </AppBar>
    </div>
  );
}
