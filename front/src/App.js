import React from 'react';
import { Route, Switch, BrowserRouter } from 'react-router-dom'
import Navbar from './components/navbar/navbar'
import './App.css';

function App() {
  return (
    <div className="App">
      <Navbar />
    </div>
  );
}

export default App;
